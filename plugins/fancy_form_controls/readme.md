Fancy Form Controls for Ferris 2
================================

This is a collection of Ferris components and wtforms classes that provide a richer set of form controls.


Usage
-----

In your controller, import the desired components


    from plugins.fancy_form_controls import DatePicker, TinyMCE, Select2


    class MyController(Controller):
        class Meta:
            components = (Scaffolding, DatePicker, TinyMCE, Select2)



Installation
------------

Either use bower or simply checkout this repository and copy it into your plugins folder. 



Copyright and license
---------------------

Copyright 2013 by Jonathan Wayne Parrott

This code is licensed under the Apache License, v2.

The third-party libraries contained (Select2, TinyMCE, and boostrap-datepicker) have varying licenses. Please refer to their license file in the /static directory for more information.
