from ferris.core.plugins import register

register('fancy_form_controls')

from .tinymce import TinyMCE
from .select2 import Select2
from .date_picker import DatePicker

__all__ = ['TinyMCE', 'Select2', 'DatePicker']
