import wtforms


class Select2(object):
    """
    Automatically turns all select boxes into nice select2 widgets.
    """

    def __init__(self, controller):
        self.controller = controller
        self.controller.meta.view.events.layout_scripts += self.layout_scripts

    def layout_scripts(self):
        return """
<link rel="stylesheet" href="/plugins/fancy_form_controls/select2/select2.css"/>
<script type="text/javascript" src="/plugins/fancy_form_controls/select2/select2.min.js"></script>
<script type="text/javascript">$(function() { $("select").select2({width: '100%'}); });</script>
"""
