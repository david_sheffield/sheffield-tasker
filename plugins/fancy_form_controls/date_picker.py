import wtforms


class DatePicker(object):
    """
    Automatically adds datepickers to date fields.
    """

    def __init__(self, controller):
        self.controller = controller
        self.controller.meta.view.events.layout_scripts += self.layout_scripts

    def layout_scripts(self):
        return """
<link rel="stylesheet" href="/plugins/fancy_form_controls/datepicker/css/datepicker.css"/>
<script type="text/javascript" src="/plugins/fancy_form_controls/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">$(function() { $('.field-type-date-field').datepicker({format: 'yyyy-mm-dd'}); });</script>
"""
