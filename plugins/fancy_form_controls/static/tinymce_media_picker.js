(function(window,document,undefined){

    var bind_update_field = function(win, field_name){
        return function(val){
            win.document.getElementById(field_name).value = val;
            // are we an image browser
            if (typeof(win.ImageDialog) != "undefined") {
                // we are, so update image dimensions...
                if (win.ImageDialog.getImageData) win.ImageDialog.getImageData();
                // ... and preview if necessary
                if (win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(val);
            }
        };
    };


    var hide_popups = function(){
        var wm = window.tinymce.activeEditor.windowManager;
        this._hidden_windows = [];
        this._hidden_windows = wm.windows.map(function(win){
            if(win._visible) win.hide();
            this._hidden_windows.push(win);
            return win;
        });
    };

    var show_popups = function(){
        var wm = window.tinymce.activeEditor.windowManager;
        this._hidden_windows.forEach(function(winx){
            if(winx.show) winx.show();
        });
    };

    var file_browser_callback = function(field_name, url, type, win){
        if(type == 'image'){
            create_photo_picker(bind_update_field(win, field_name));
        } else if(type == 'media'){
            create_media_picker(bind_update_field(win, field_name));
        } else {
            create_file_picker(bind_update_field(win, field_name));
        }
    };

    var create_photo_picker = function(update_field){
        var view = new google.picker.PhotosView();
        var upload_view = new google.picker.View(google.picker.ViewId.PHOTO_UPLOAD);
        var search_view = new google.picker.ImageSearchView();

        var picker = new google.picker.PickerBuilder()
        .addView(view).addView(upload_view).addView(search_view).
        addView(google.picker.ViewId.RECENTLY_PICKED).
        setCallback(function(data){
            if (data.action == google.picker.Action.PICKED) {
                var doc = data.docs[0];
                if(doc.thumbnails){
                    update_field(doc.thumbnails.pop().url);
                } else {
                    update_field(doc.embedUrl||doc.url);
                }
            }
            if(data.action == google.picker.Action.PICKED || data.action == google.picker.Action.CANCEL){
                show_popups();
            }
        }).
        build();

        hide_popups();
        picker.setVisible(true);
    };


    var create_media_picker = function(update_field){
        var view = new google.picker.PhotosView();
        var upload_view = new google.picker.View(google.picker.ViewId.PHOTO_UPLOAD);
        var search_view = new google.picker.ImageSearchView();

        var picker = new google.picker.PickerBuilder().
        addView(google.picker.ViewId.YOUTUBE).
        addView(view).addView(upload_view).addView(search_view).
        setCallback(function(data){
            if (data.action == google.picker.Action.PICKED) {
                var doc = data.docs[0];
                update_field(doc.url);
            }
            if(data.action == google.picker.Action.PICKED || data.action == google.picker.Action.CANCEL){
                show_popups();
            }
        }).
        build();

        hide_popups();
        picker.setVisible(true);

    };


    var create_file_picker = function(update_field){
        var views = [];
        var view_groups = [];

        var docs_view = new google.picker.DocsView();
        docs_view.setIncludeFolders(true);
        docs_view.setSelectFolderEnabled(true);

        var docs_upload_view = new google.picker.DocsUploadView();
        docs_upload_view.setIncludeFolders(true);

        var docs_view_group = new google.picker.ViewGroup(docs_view);

        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.DOCUMENTS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.PRESENTATIONS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.SPREADSHEETS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.FORMS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.DOCS_VIDEOS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.DOCS_IMAGES));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.DRAWINGS));
        docs_view_group.addView(new google.picker.DocsView(google.picker.ViewId.FOLDERS));
        docs_view_group.addView(docs_upload_view);
        view_groups.push(docs_view_group);

        var image_view_group = new google.picker.ViewGroup(new google.picker.PhotosView());
        image_view_group.addView(google.picker.ViewId.PHOTO_UPLOAD);
        image_view_group.addView(new google.picker.ImageSearchView());
        view_groups.push(image_view_group);

        var picker = new google.picker.PickerBuilder();

        view_groups.forEach(function(vg){
            picker.addViewGroup(vg);
        });

        picker.addView(google.picker.ViewId.RECENTLY_PICKED);
        picker.setCallback(function(data){
            if (data.action == google.picker.Action.PICKED) {
                var doc = data.docs[0];
                update_field(doc.url);
            }
            if(data.action == google.picker.Action.PICKED || data.action == google.picker.Action.CANCEL){
                show_popups();
            }
        });
        
        picker = picker.build();

        hide_popups();
        picker.setVisible(true);
    };


    window.gapi.load('picker', '1');
    window.tinymce_google_picker = file_browser_callback;
})(window, document);
