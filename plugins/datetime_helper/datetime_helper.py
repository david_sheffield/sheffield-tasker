import datetime

def get_time_left_text(timestamp,tz_offset=0):
    now = datetime.datetime.now()
    user_time = timestamp + datetime.timedelta(hours=tz_offset)
    return_text = ''
    if timestamp<now:
        return_text += ' Late!!! (%s)' % (user_time.strftime('%m/%d/%Y %I:%M %p'))
    else:
        diff = timestamp - now
        days_diff = diff.days
        seconds_diff = diff.seconds
        hours_diff = seconds_diff/3600
        minutes_diff = seconds_diff/60
        if days_diff >= 14:
            weeks_left = days_diff/7
            return_text += " in %s weeks (%s)" % (weeks_left,
                user_time.strftime('%a, %b %d'))
        if days_diff < 14 and days_diff >= 7:
            return_text += " in %s days (%s)" % (days_diff,
                user_time.strftime('%a, %b %d'))
        if days_diff < 7 and days_diff > 1:
            return_text += " %s" % (user_time.strftime('%A, %I:%M %p'))
        if days_diff == 1:
            return_text += " Tomorrow, %s" % (user_time.strftime('%I:%M %p'))
        if days_diff == 0:
            if hours_diff > 7:
                return_text += " in %s hours, %s" % (hours_diff,
                    user_time.strftime('%I:%M %p'))
            else:
                if hours_diff == 1:
                    return_text += ' in 1 hour (%s)' % (
                        user_time.strftime('%I:%M %p'))
                if hours_diff == 0:
                    return_text += ' in %s minutes (%s)' % (minutes_diff,
                        user_time.strftime('%I:%M %p'))
            
        
    return return_text
    