"""
Central place to store event listeners for your application,
automatically imported at run time.
"""
import logging
import urllib2
from app.models.user_pref import UserPref
from ferris.core.events import on
from google.appengine.api import users


# example
@on('controller_before_authorization')
def inject_authorization_chains(controller, authorizations):
    #authorizations.insert(0, custom_auth_chain)
    pass
    
@on('controller_before_startup')
def before_startup(controller, *args, **kwargs):
    controller.session['safe_url'] = urllib2.quote(controller.request.url)
    user = users.get_current_user()
    if user:
        #is user in db
        db_user_pref = UserPref.get_user_pref(user)
        if not db_user_pref:
            db_user_pref = UserPref(user=user,email=user.email()).put()
        if 'user_pref' not in controller.session:
            controller.session['user_pref'] = db_user_pref
        if 'tz_offset' not in controller.session:
            controller.session['tz_offset'] = UserPref.get_user_tz_offset(user)
            
