from ferris import BasicModel, ndb
import datetime

class UserPref(BasicModel):
    user = ndb.UserProperty()
    email = ndb.StringProperty()
    tz_offset = ndb.IntegerProperty()
    adjust_for_dst = ndb.BooleanProperty()
    
    @classmethod
    def get_user_tz_offset(cls,user):
        qry = cls.find_all_by_properties(user=user)
        if qry.count() < 1:
            return 0
        else:
            results = qry.fetch()
            if results[0].adjust_for_dst:
                dst_start = datetime.datetime.strptime('03-09-2014', '%m-%d-%Y')
                dst_end = datetime.datetime.strptime('11-02-2014', '%m-%d-%Y')
                today = datetime.datetime.today()
                if today >= dst_start and today < dst_end:
                    return results[0].tz_offset + 1
                else:
                    return results[0].tz_offset
            else:
                return results[0].tz_offset
    
    @classmethod
    def get_user_pref(cls,user):
        user_pref = cls.query(cls.user==user)
        user_pref = user_pref.fetch(limit=1)
        if user_pref:
            return user_pref[0]
        else:
            return None