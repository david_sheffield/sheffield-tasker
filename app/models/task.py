from app.models.reminder import Reminder
from app.models.subtask import Subtask
from ferris import BasicModel, ndb
from google.appengine.api import users

class Task(BasicModel):
    name = ndb.StringProperty(required=True)
    category = ndb.StringProperty(required=True,default='MyTasks')
    priority = ndb.IntegerProperty()
    is_open = ndb.BooleanProperty(required=True,default=True)
    user = ndb.UserProperty()
    closed_date_time = ndb.DateTimeProperty()
    due_date_time = ndb.DateTimeProperty()
    past_due_notification_sent = ndb.BooleanProperty()
    
    def after_put(self,key):
        if self.is_open==False:
            #mark all subtasks complete
            subtasks = Subtask.query(Subtask.task_key==self.key,
                Subtask.is_open==True)
            subtasks = subtasks.fetch()
            for item in subtasks:
                item.is_open = False
                item.put()
            #delete any open reminders
            reminders = Reminder.get_all_reminders_for_task(self.key)
            for rem in reminders:
                rem.key.delete()
                
    @classmethod
    def before_delete(cls,key):
        #delete all subtasks
        subtasks = Subtask.query(Subtask.task_key==key)
        for item in subtasks:
            item.key.delete()
        #delete all reminders
        reminders = Reminder.get_all_reminders_for_task(key)
        for rem in reminders:
            rem.key.delete()
    
    def before_put(self):
        user = users.get_current_user()
        if not self.priority:
            self.priority = Task.get_lowest_priority(user)
        if not self.user:
            self.user = user
            
    @classmethod
    def get_after_item_priority(cls,key):
        item = key.get()
        qry = cls.query(cls.priority > item.priority).order(cls.priority)
        results = qry.fetch(limit=1)
        if not results:
            return item.priority + 1000
        else:
            priority_diff = results[0].priority - item.priority
            return item.priority + (priority_diff/2)
            
    @classmethod
    def get_basic_info_for_email(cls,task_key):
        item = task_key.get()
        body = ''
        html_body = ''
        body += "%s (%s)\n---------------------\n" % (item.name,item.category)
        html_body += "<h3>%s (%s)</h3><hr />" % (item.name,item.category)
        subtasks = Subtask.get_subtasks(task_key)
        if subtasks:
            body+='Subtasks:\n'
            html_body += '<h4>Subtasks</h4>'
            for sub in subtasks:
                body += '%s\n' % (sub.name)
                html_body += '%s<br />' % (sub.name)
        return {'body':body,'html_body':html_body,}
        
                        
    @classmethod
    def get_lowest_priority(cls,user):
        lowest_item = cls.query(cls.user==user).order(cls.priority)
        lowest_item = lowest_item.fetch(limit=1)
        if lowest_item:
            priority_to_return = lowest_item[0].priority-100000
        else:
            priority_to_return = 100000
        return priority_to_return
        
    @classmethod
    def get_user_tasks(cls,user,is_open=True,category=None):
        qry = cls.query(cls.user==user,cls.is_open==is_open)
        if is_open == True:
            qry = qry.order(cls.priority)
        if is_open == False:
            qry = qry.order(-cls.closed_date_time)
        if category:
            qry = qry.filter(cls.category==category)
        results = qry.fetch()
        return results
        
    @classmethod
    def move_task(cls,key,direction):
        item = key.get()
        if direction=='up':
            qry = cls.query(cls.user==item.user,cls.priority<item.priority).order(cls.priority)
            results = qry.fetch(limit=1)
        if direction=='down':
            qry = cls.query(cls.user==item.user,cls.priority>item.priority).order(cls.priority)
            results = qry.fetch(limit=1)
        if not results:
            return None
        else:
            priority1 = item.priority
            priority2 = results[0].priority
            item.priority = priority2
            item.put()
            item2 = results[0].key.get()
            item2.priority = priority1
            item2.put()
            return True
            
    @classmethod
    def move_task_to_top(cls,key):
        item = key.get()
        qry = cls.query(cls.is_open==True,cls.user==item.user).order(
            cls.priority)
        results = qry.fetch(limit=1)
        if not results:
            item.priority = 100000
        else:
            item.priority = results[0].priority - 100000
        item.put()