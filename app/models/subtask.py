from ferris import BasicModel, ndb

class Subtask(BasicModel):
    name = ndb.StringProperty(required=True)
    task_key = ndb.KeyProperty()
    priority = ndb.IntegerProperty()
    is_open = ndb.BooleanProperty(default=True)
    
    def before_put(self):
        if not self.priority:
            qry = Subtask.query(Subtask.task_key==self.task_key).order(
                -Subtask.priority)
            results = qry.fetch(limit=1)
            if not results:
                self.priority = 100000
            else:
                self.priority = results[0].priority+100000
    
    @classmethod
    def get_subtasks(cls,task_key):
        qry = cls.query(cls.task_key==task_key).order(cls.priority)
        results = qry.fetch()
        return results