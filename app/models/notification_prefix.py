from ferris import BasicModel, ndb
import logging
import random

class NotificationPrefix(BasicModel):
    prefix_name = ndb.StringProperty(required=True,choices=['past_due',
        'email_reminder'])
    prefix = ndb.StringProperty(required=True)
    
    @classmethod
    def get_list_of_prefixes(cls,prefix_name,number_needed=1):
        qry = cls.query(cls.prefix_name==prefix_name)
        results = qry.fetch()
        if results:
            list_to_return = []
            total_results = len(results)
            count = 0
            while count < number_needed:
                random_number = random.randint(1, total_results)
                list_to_return.append(results[random_number-1].prefix)
                count = count + 1
            return list_to_return 
        else:
            return None
            
    @classmethod
    def get_random_prefix(cls,prefix_name):
        qry = cls.query(cls.prefix_name==prefix_name)
        results = qry.fetch()
        if results:
            random_number = random.randint(1, len(results))
            return results[random_number-1].prefix
        else:
            return ""