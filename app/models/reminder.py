from ferris import BasicModel, ndb

class Reminder(BasicModel):
    reminder_type = ndb.StringProperty(required=True,choices=['email'],
        default='email')
    reminder_date_time = ndb.DateTimeProperty()
    is_pending = ndb.BooleanProperty(default=True)
    task_key = ndb.KeyProperty()
    user = ndb.UserProperty()
    subject = ndb.StringProperty()
    body = ndb.TextProperty()
    html_body = ndb.TextProperty()
    
    @classmethod
    def get_pending_reminders(cls,user):
        qry = cls.query(cls.is_pending==True,cls.user==user)
        results = qry.fetch()
        return results
        
    @classmethod
    def get_all_reminders_for_task(cls,task_key):
        qry = cls.query(cls.task_key==task_key).order(
            cls.reminder_date_time)
        results = qry.fetch()
        return results
        
    @classmethod
    def get_pending_reminders_for_task(cls,task_key):
        qry = cls.query(cls.is_pending==True,cls.task_key==task_key).order(
            cls.reminder_date_time)
        results = qry.fetch()
        return results