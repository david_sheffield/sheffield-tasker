from app.models.notification_prefix import NotificationPrefix
from app.models.reminder import Reminder
from app.models.subtask import Subtask
from app.models.task import Task
from ferris import Controller, route, scaffold
from ferris.components.flash_messages import FlashMessages
from ferris.core.forms import model_form
from google.appengine.api import mail
from plugins.datetime_helper import datetime_helper

import datetime
import logging

#form for base add
class BaseTaskAdd(model_form(Task,only=['name','category'])):
    pass

#form for base edit
class BaseTaskEdit(model_form(Task,only=['name','category'])):
    pass

class Tasks(Controller):
    class Meta:
        components = (FlashMessages,scaffold.Scaffolding,)
        prefixes = ('admin','cron',)
        
    admin_list = scaffold.list        #lists all posts
    admin_view = scaffold.view        #view a post
    admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post
    
    def add(self):
        def set_redirect(controller, container, item):
            controller.scaffold.redirect = controller.uri(action='my_view',
                key=item.key.urlsafe())
        
        self.scaffold.ModelForm = BaseTaskAdd
        self.events.scaffold_after_save += set_redirect        
        return scaffold.add(self)
        
    @route
    def cron_notify_past_due(self):
        now = datetime.datetime.now()
        qry = self.meta.Model.query(self.meta.Model.due_date_time<=now,
            self.meta.Model.past_due_notification_sent==False)
        results = qry.fetch(limit=10)
        list_of_prefixes = NotificationPrefix.get_list_of_prefixes('past_due',
            len(results))
        logging.info(list_of_prefixes)
        count = 0
        for item in results:
            self.meta.Model.move_task_to_top(item.key)
            item.past_due_notification_sent = True
            item.put()
            email_bodies = self.meta.Model.get_basic_info_for_email(item.key)
            subject = "%s %s" % (list_of_prefixes[count],item.name)
            mail.send_mail('jeremiah.sheffield@gmail.com',
                item.user.email(), subject, email_bodies['body'],
                html=email_bodies['html_body'])
            count = count + 1
        return "%s past due items moved" % (len(results))
        
    def delete(self,key):
        self.scaffold.redirect = self.uri('tasks:my')
        return scaffold.delete(self,key)
        
    @route
    def mark_complete(self,key):
        item=self.util.decode_key(key).get()
        item.is_open = False
        item.closed_date_time = datetime.datetime.now()
        item.put()
        FlashMessages(self).flash('Item Marked Complete', type='info')
        return self.redirect(self.uri('tasks:my'))
    
    @route
    def move(self,key,direction):
        item_key=self.util.decode_key(key)
        move_task = self.meta.Model.move_task(item_key,direction)
        if move_task:
            FlashMessages(self).flash('Item Moved', type='info')
        return self.redirect(self.uri('tasks:my'))
    
    @route
    def my(self):
        status = self.request.get('status','Open')
        category = self.request.get('category',None)
        self.context['category'] = category
        if status == 'Open':
            is_open = True
        else:
            is_open = False
        self.context['is_open'] = is_open
        self.context['results'] = self.meta.Model.get_user_tasks(self.user,
            is_open,category)
        if category:
            self.context['closed_results'] = self.meta.Model.get_user_tasks(
                self.user,False,category)
        self.context['get_time_left_text'] = datetime_helper.get_time_left_text
            
    @route
    def my_add(self): 
        form = BaseTaskAdd()
        self.context['form'] = form
        parsed_form = self.parse_request(container=form)
        if self.request.method != 'GET':
            if parsed_form.validate():
                new = self.meta.Model()
                new.name = self.request.get('name')
                new.category = self.request.get('category')
                if self.request.get('after_item'):
                    after_item_key = self.util.decode_key(
                        self.request.get('after_item'))
                    priority = self.meta.Model.get_after_item_priority(
                        after_item_key)
                    new.priority = priority
                new.put()
                FlashMessages(self).flash('Task Added', type='info')
                return self.redirect(self.uri('tasks:my_view',
                    key=new.key.urlsafe()))
    
    @route
    def my_edit(self,key):
        item = self.util.decode_key(key).get()
        edit_form = BaseTaskEdit()
        self.context['edit_form'] = edit_form
        self.context['item']=item
        parsed_form = self.parse_request(container=edit_form, fallback=item)
        if self.request.method != 'GET':
            logging.info(parsed_form.data)
            if parsed_form.validate():
                item.name = parsed_form.data['name']
                item.category = parsed_form.data['category']
                item.put()
                FlashMessages(self).flash('Item Updated', type='info')
                return self.redirect(self.uri('tasks:my_view',
                    key=item.key.urlsafe()))
                    
    @route
    def my_view(self,key):
        item = self.util.decode_key(key).get()
        self.context['item']=item
        self.context['subtasks'] = Subtask.get_subtasks(item.key)
        self.context['reminders'] = Reminder.get_pending_reminders_for_task(
            item.key)
        self.context['get_time_left_text'] = datetime_helper.get_time_left_text
            
    @route
    def set_due_date_time(self,key):
        item = self.util.decode_key(key).get()
        self.context['item']=item
        if item.due_date_time:
            user_due_date_time = item.due_date_time + datetime.timedelta(
                hours=self.session['tz_offset'])
            self.context['user_date'] = user_due_date_time.strftime('%Y-%m-%d')
            self.context['user_time'] = user_due_date_time.strftime('%H:%M')
        if self.request.method != 'GET':
            form_date = self.request.get('datepicker')
            form_time = self.request.get('timepicker')
            form_datetime = "%s %s" % (form_date,form_time)
            user_datetime = datetime.datetime.strptime(form_datetime,
                '%Y-%m-%d %H:%M')
            utc_datetime = user_datetime - datetime.timedelta(
                hours=self.session['tz_offset'])
            item.due_date_time = utc_datetime
            item.past_due_notification_sent = False
            item.put()
            FlashMessages(self).flash('Due Date Time Set', type='info')
            return self.redirect(self.uri('tasks:my_view',
                key=item.key.urlsafe()))
            
            
    @route
    def swap_tasks(self):
        item1 = self.util.decode_key(self.request.get('item1')).get()
        item2 = self.util.decode_key(self.request.get('item2')).get()
        pri_1 = item1.priority
        pri_2 = item2.priority
        item1.priority = pri_2
        item2.priority = pri_1
        item1.put()
        item2.put()
        FlashMessages(self).flash('Item Moved', type='info')
        return self.redirect(self.uri('tasks:my'))