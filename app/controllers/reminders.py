from app.models.notification_prefix import NotificationPrefix
from app.models.task import Task
from ferris import Controller, route, scaffold
from ferris.components.flash_messages import FlashMessages
from google.appengine.api import mail
import datetime
import logging

class Reminders(Controller):
    class Meta:
        components = (FlashMessages,scaffold.Scaffolding,)
        prefixes = ('admin','cron',)
        
    admin_list = scaffold.list        #lists all posts
    admin_view = scaffold.view        #view a post
    admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post
    
    @route
    def add_reminder(self,task_key):
        task_key = self.util.decode_key(task_key)
        task = task_key.get()
        self.context['task_key'] = task_key
        if self.request.method != 'GET':
            form_date = self.request.get('datepicker')
            form_time = self.request.get('timepicker')
            form_datetime = "%s %s" % (form_date,form_time)
            user_datetime = datetime.datetime.strptime(form_datetime,
                '%Y-%m-%d %H:%M')
            utc_datetime = user_datetime - datetime.timedelta(
                hours=self.session['tz_offset'])
            new = self.meta.Model()
            new.reminder_date_time = utc_datetime
            new.task_key = task_key
            new.user = self.user
            prefix = NotificationPrefix.get_random_prefix('email_reminder')
            new.subject = "%s %s" % (prefix,task.name)
            new.body = "Don't forget about %s\n---------------------\n\n" % (
                task.name)
            new.html_body = "<h2>Don't forget about %s</h2><hr />" % (task.name)
            new.put()
            FlashMessages(self).flash('Reminder Added', type='info')
            return self.redirect(self.uri('tasks:my_view',
                key=task.key.urlsafe()))
                
    @route
    def cron_send_pending_emails(self):
        now = datetime.datetime.now()
        qry = self.meta.Model.query(self.meta.Model.reminder_date_time<=now,
            self.meta.Model.is_pending==True,
            self.meta.Model.reminder_type=='email')
        results = qry.fetch(limit=10)
        logging.info('Total Reminders to send: %s' % (len(results)))
        for item in results:
            task_record = item.task_key.get()
            if task_record.is_open:
                task_info = Task.get_basic_info_for_email(item.task_key)
                body = item.body + task_info['body']
                html_body = item.html_body + task_info['html_body']
                mail.send_mail('jeremiah.sheffield@gmail.com',
                    item.user.email(), item.subject, body, html=html_body)
                item.key.delete()
            else:
                item.key.delete()
        return 'Tried to send %s emails' % (len(results))
        
                
    def delete(self,key):
        item = self.util.decode_key(key).get()
        self.scaffold.redirect = self.uri('tasks:my_view',
            key=item.task_key.urlsafe())
        return scaffold.delete(self,key)
            