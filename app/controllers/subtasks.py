from app.models.subtask import Subtask
from app.models.task import Task
from ferris import Controller, route, scaffold
from ferris.components.flash_messages import FlashMessages
from ferris.core.forms import model_form

import logging

#form for base add
class BaseSubtaskAdd(model_form(Subtask,exclude=['task_key','priority'])):
    pass

class Subtasks(Controller):
    class Meta:
        components = (FlashMessages,scaffold.Scaffolding,)
        prefixes = ('admin',)
        
    @route
    def add_subtask(self,task_key):
        task_key = self.util.decode_key(task_key)
        self.context['task_key'] = task_key
        add_form = BaseSubtaskAdd()
        self.context['add_form'] = add_form
        if self.request.method != 'GET':
            parsed_form = self.parse_request(container=add_form)
            logging.info(parsed_form.data)
            if parsed_form.validate():
                self.meta.Model(name=parsed_form.data['name'],
                    task_key=task_key).put()
                FlashMessages(self).flash('Item Added', type='info')
                return self.redirect(self.uri('tasks:my_view',
                    key=task_key.urlsafe()))
                    
    def delete(self,key):
        item = self.util.decode_key(key).get()
        self.scaffold.redirect = self.uri('tasks:my_view',
            key=item.task_key.urlsafe())
        return scaffold.delete(self,key)
                    
    @route
    def toggle_subtask(self,key):
        item = self.util.decode_key(key).get()
        if item.is_open:
            item.is_open = False
        else:
            item.is_open = True
        item.put()
        FlashMessages(self).flash('Sub Task Updated', type='info')
        return self.redirect(self.uri('tasks:my_view',
            key=item.task_key.urlsafe()))